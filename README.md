# Flowable-Bpmn-Design

> :star: 代码还未上传，最近忙于工作，抽空再搞...  :star: 

> :sparkles: 有需要做定制开发的，私信我 :sparkles: 

### 介绍
基于 `bpmn-js ^13.2.2` 封装的流程设计器。。。
适用于 `flowable` `activiti` `camunda`，其它流程引擎未测试

> 这里面主要包含两个组件，流程设计`cpt-flow-design`，流程预览`cpt-flow-viewer`。效果图在下面！！！



### 软件架构
| 依赖                             | 版本   |
|--------------------------------|---------|
| bpmn-js                        | ^13.2.2 |
| bpmn-js-bpmnlint               | ^0.21.0 |
| bpmn-js-color-picker           | ^0.6.0  |
| bpmn-js-create-append-anything | ^0.3.0  |
| bpmn-js-token-simulation       | ^0.31.1 |
| bpmnlint                       | ^9.1.0  |
| diagram-js                     | ^12.2.0 |
| diagram-js-minimap             | ^4.1.0  |
| element-ui                     | ^2.15.6 |
| highlight.js                   | ^10.7.3 |
| vue                            | ^2.6.10 |


### 运行
npm run serve / yarn serve

### 打包
npm run plug / yarn plug


### 使用

```javascript
// 引入js，放在vue.js加载后，组件会自动注册
<script src="${你的路径}/index.umd.min.js" charset="utf-8"></script>
```

```html
// 使用组件
<cpt-flow-design></cpt-flow-design>
```


### 配置项

#### Props 配置
|参数		|描述		|类型	|是否必须|可选值		|默认值	|
|-		|-		|-	|-	|-		|-	|
|prefix		|适用流程引擎	|String	|否	|flowable、activiti、camunda	|flowable|
|processId	|流程的key	|String	|否	|-		|-	|
|processName	|流程的名称	|String	|否	|-		|-	|
|processXml	|流程xml字符串	|String	|否	|-		|-	|
|formKey	|内置表单key(与extFormKey只能传一个，否则默认使用formKey)	|String	|否	|-		|-	|
|extFormKey	|外置表单key(与formKey只能传一个，否则默认使用extFormKey)	|String	|否	|-		|-	|
|forms		|节点表单数据源	|Array	|否	|		|[]	|
|buttons	|节点按钮数据源	|Array	|否	|		|[]	|
|functions	|功能数据源	|Array	|否	|		|[{title: "系统功能",funcs: []}, {title: "自定义功能", funcs: []}];	|
|taskListeners	|任务监听器数据源	|Array	|否	|		|[]	|
|executionListeners|执行监听器数据源|Array	|否	|		|[]	|
|events		|注册事件	|Array	|否	|bpmn所有事件	|["element.click"]|
|panelWidth	|右侧边操作栏宽度|Number	|否	|		|400	|
|drawerWidth	|右侧边操作栏所有抽屉宽度	|Number	|否	|	|500	|


#### functions 功能集合
|参数		|说明		|类型	|是否必须|可选值		|
|-		|-		|-	|-	|-		|
|title		|功能名称	|String	|是	|-		|
|funcs		|功能标识	|Array	|否	|-		|

#### funcs 功能标识
|参数		|说明		|类型	|是否必须|可选值		|
|-		|-		|-	|-	|-		|
|name		|功能标识名称	|String	|是	|-		|
|code		|功能标识code	|String	|是	|-		|

#### forms 节点表单
|参数		|说明		|类型	|是否必须|可选值		|
|-		|-		|-	|-	|-		|
|name		|表单名		|String	|是	|-		|
|code		|表单标识	|String	|是	|-		|
|fields		|字段		|Array	|否	|-		|

#### fields 节点表单字段
|参数		|说明		|类型	|是否必须|可选值		|
|-		|-		|-	|-	|-		|
|name		|字段名		|String	|是	|-		|
|prop		|字段属性	|String	|是	|-		|
|readable	|可读		|Boolean|否	|-		|
|writable	|可写		|Boolean|否	|-		|
|required	|必填		|Boolean|否	|-		|

#### buttons 节点按钮
|参数		|说明		|类型	|是否必须|可选值		|
|-		|-		|-	|-	|-		|
|name		|按钮名		|String	|是	|-		|
|code		|按钮标识	|String	|是	|-		|
|display	|是否显示	|Boolean|是	|-		|

#### taskListeners/executionListeners 监听器
|参数	|说明		|类型	|是否必须|可选值		|
|-	|-		|-	|-	|-		|
|id	|id标识		|String	|否	|-		|
|type	|监听器类型	|String	|是	|classListener、expressionListener、delegateExpressionListener|
|name	|监听器名称	|String	|是	|-		|
|value	|Java类全限定名、表达式、代理表达式|String|是	|-		|
|params	|参数		|Array	|否	|-		|

#### params 监听器参数字段
|参数	|说明		|类型	|是否必须|可选值		|
|-	|-		|-	|-	|-		|
|id	|id标识		|String	|否	|-		|
|type	|参数类型	|String	|是	|string、expression|
|name	|参数名	|String	|是	|-		|
|value	|字符串、表达式	|String	|是	|-		|


#### user/role/dept/post
|参数	|说明		|类型	|是否必须|可选值		|
|-	|-		|-	|-	|-		|
|text	|展示名		|String	|是	|-		|
|value	|值		|String	|是	|-		|


#### Events 事件
|events（包含events注册的事件）	|说明			|参数	|
|-				|-			|-	|
|init-finished			|初始化完成事件		|Modeler|
|destroy			|销毁事件		|Modeler|
|commandStack-changed		|图形改变事件		|event	|
|change				|图形改变事件		|xml	|
|shape-create			|图形创建事件		|context|
|import-done			|导入完成事件		|	|
|-				|-			|-	|
|select-user			|选择用户事件		|data, resolve|
|select-role			|选择角色事件		|data, resolve|
|select-dept			|选择部门事件		|data, resolve|
|select-post			|选择岗位事件		|data, resolve|



#### Method 方法
|method		|说明			|参数		|返回值		|
|-		|-			|-		|-		|
|createDiagram	|重置流程图		|(xml 选传)	|		|
|importXML	|导入xml		|(xml)		|		|
|getBpmnModeler	|获取流程模型对象Modeler	|		|Modeler	|
|getModelData	|获取流程信息		|		|{id: 'xxx', name: 'xxx', ...}	|
|getXml		|获取xml		|		|xml	|
|-		|-			|-		|-	|
|updateUserTaskFormKey|更新流程用户任务节点formKey		|formKey	|	|
|updateUserTaskExtFormKey|更新流程用户任务节点formKey		|extFormKey	|	|
|addOrUpdateUserTaskButton|新增或更新流程用户任务节点button	|buttons	|	|
|deleteAllUserTaskButton|删除所有用户任务节点button		|		|	|
|addOrUpdateAllFormProperty|新增或更新流程节点FormProperty	|fields		|	|
|deleteAllFormProperty|删除所有节点FormProperty		|		|	|
|-				|-			|-	|
|updateUserTaskIdmAssignee|更新用户任务节点，身份存储 > 分配人员|user		|	|
|updateUserTaskIdmUser|更新用户任务节点，身份存储 > 候选人员|user数组		|	|
|updateUserTaskIdmRole|更新用户任务节点，身份存储 > 候选角色|role数组		|	|
|updateUserTaskIdmDept|更新用户任务节点，身份存储 > 候选部门|dept数组		|	|
|updateUserTaskIdmPost|更新用户任务节点，身份存储 > 候选岗位|post数组		|	|

#### Slot 插槽
|slot| 				|
|-		|-			|
|name		|说明			|
|toolbar	|左侧功能按钮区的右边	|
|toolbar-left	|右侧功能按钮区的左边	|
|toolbar-right	|右侧功能按钮区的右边	|



### 效果图（简单截了几张）
![流程设计器](imgs%E6%B5%81%E7%A8%8B%E8%AE%BE%E8%AE%A1%E5%9B%BE.png)
![开始节点配置](imgs%E5%BC%80%E5%A7%8B%E8%8A%82%E7%82%B9%E9%85%8D%E7%BD%AE.png)
![用户节点配置](imgs%E7%94%A8%E6%88%B7%E8%8A%82%E7%82%B9%E9%85%8D%E7%BD%AE.png)
![用户节点审批人配置](imgs%E7%94%A8%E6%88%B7%E8%8A%82%E7%82%B9%E5%AE%A1%E6%89%B9%E4%BA%BA%E9%85%8D%E7%BD%AE.png)
![用户节点会签配置](imgs%E4%BC%9A%E7%AD%BE%E5%8A%9F%E8%83%BD%E9%85%8D%E7%BD%AE.png)
![用户节点表单配置](imgs%E8%A1%A8%E5%8D%95%E9%85%8D%E7%BD%AE.png)

### 运用到系统中的效果图（流程设计器中其实有很多小细节配置，这里就不一 一展示了，简单展示两张）
#### 内置表单
![内置表单](imgs/%E6%94%AF%E6%8C%81%E7%9A%84%E8%A1%A8%E5%8D%95%E7%B1%BB%E5%9E%8B.png)
#### 内置表单配置
![内置表单配置](imgs/%E8%A1%A8%E5%8D%95_%E6%8C%89%E9%92%AE%E9%85%8D%E7%BD%AE.png)

#### 外置表单
![外置表单](imgs/%E5%A4%96%E7%BD%AE%E8%A1%A8%E5%8D%95.png)
#### 外置表单配置
![外置表单配置](imgs/%E5%A4%96%E7%BD%AE%E8%A1%A8%E5%8D%95%E9%85%8D%E7%BD%AE.png)

#### 独立表单配置
![独立表单配置](imgs/%E7%8B%AC%E7%AB%8B%E8%A1%A8%E5%8D%95%E9%85%8D%E7%BD%AE.png)

#### 审批人配置
![审批人配置](imgs/%E5%AE%A1%E6%89%B9%E4%BA%BA%E9%85%8D%E7%BD%AE.png)


# Flowable-Bpmn-Viewer

### 流程预览组件

```js
// 直接使用组件
<cpt-flow-viewer :xml="modelEditorXml"></cpt-flow-viewer>

export default {
  data() {
    return {
      modelEditorXml: "你的xml",
    }
  }
}
```

### 效果图
![流程预览效果图](imgs/viewer/%E6%B5%81%E7%A8%8B%E9%A2%84%E8%A7%88%E6%95%B4%E4%BD%93%E6%95%88%E6%9E%9C%E5%9B%BE.png)

![流程浮框效果图](imgs/viewer/%E6%B5%AE%E6%A1%86%E6%95%88%E6%9E%9C%E5%9B%BE.png)

![流程模拟效果图](imgs/viewer/%E6%B5%81%E7%A8%8B%E6%A8%A1%E6%8B%9F%E6%95%88%E6%9E%9C%E5%9B%BE.png)

### 运用到系统中的效果图
![流程效果图](imgs/viewer/%E9%A2%84%E8%A7%88%E6%95%88%E6%9E%9C%E5%9B%BE.gif)
![自定义浮框效果图](imgs/viewer/%E8%87%AA%E5%AE%9A%E4%B9%89%E6%B5%AE%E6%A1%86%E6%95%88%E6%9E%9C%E5%9B%BE.png)
![序列号效果图](imgs/viewer/%E5%BA%8F%E5%88%97%E5%8F%B7%E6%95%88%E6%9E%9C%E5%9B%BE.gif)
![序列号效果图2](imgs/viewer/%E5%BA%8F%E5%88%97%E5%8F%B7%E6%95%88%E6%9E%9C%E5%9B%BE2.gif)

示例代码
```js
// 这里为了方便展示，移除了xml等配置
<cpt-flow-viewer :node-infos="nodeInfos" :node-info-style="nodeInfoStyle" />

export default {
  data() {
    return {
      nodeInfos: [
	{
	  id: 'StartEvent_1', 
	  html: '123123', 
	  style: {left: -200, width: '500px'}
	}
      ],
      nodeInfoStyle: {
	class: 'cutstom-class',
	style: 'color: red;'
      }
    }
  }
}

```


### Props 配置
|参数		|描述		|类型	|是否必须|可选值		|默认值	|
|-		|-		|-	|-	|-		|-	|
|xml		|流程的xml字符串	|String	|是	|-		|-	|
|loadZoomScrollModule 	|加载缩放滚动模块	|Boolean|否	|true/false	|true	|
|loadMoveCanvasModule 	|加载拖拽模块	|Boolean|否	|true/false	|true	|
|loadSimulationModule 	|加载模拟模块	|Boolean|否	|true/false	|false	|
|loadMinimapModule 	|加载小地图模块	|Boolean|否	|true/false	|false	|
|loadSerialNumber	|加载序号	|Boolean|否	|true/false	|false	|
|openSimulation 	|默认打开模拟（加载了loadSimulationModule有效）	|Boolean|否	|true/false	|true	|
|openMinimap 	|默认打开小地图（加载了loadMinimapModule有效）	|Boolean|否	|true/false	|true	|
|openSerialNumber|默认显示序号	|Boolean|否	|true/false	|false	|
|adaptiveScreen	|自适应屏幕并居中|Boolean|否	|true/false	|false	|
|toolbarDisplay	|工具栏显隐	|Boolean|否	|true/false	|true	|
|toolbarPosition|工具栏位置	|String	|否	|top/top-start/top-end/bottom/bottom-start/bottom-end/left/left-start/left-end/right/right-start/right-end	|left	|
|nodeColors	|节点颜色参数	|Array	|否	|[{id: 节点id, fill: 背景颜色，stroke： 边框颜色}]	|[]	|
|nodeInfos	|节点信息参数	|Array	|否	|[{id: 节点id, html: html代码/dom元素, style: nodeInfoStyle}]	|[]	|
|nodeInfoStyle	|节点信息样式全局配置	|Object	|否	|-	|{}	|
|modules	|模块扩展（只要bpmn-js的Viewer支持的都可以）|Array	|否	|-	|[]	|

#### nodeColor 节点颜色参数
|参数		|说明			|类型	|是否必须|可选值		|
|-		|-			|-	|-	|-		|
|id		|节点id			|String	|是	|-		|
|fill		|背景颜色		|String	|否	|-		|
|stroke		|边框颜色		|String	|否	|-		|


#### nodeInfo 节点信息参数
|参数	|说明				|类型	|是否必须|可选值		|
|-	|-				|-	|-	|-		|
|id	|节点id				|String	|是	|-		|
|sort	|节点序号			|Number	|否	|-		|
|html	|html代码或纯dom元素，例如通过document.createElement('div')创建dom		|String，Object，HTMLElement	|否	|-		|
|style	|节点样式配置，优先级大于 nodeInfoStyle 	|Object	|否	|参考 nodeInfoStyle |
|comments|节点comments，例如：['xxx', 'xxx',...]	|Array	|否	|-	|
|fill	|背景颜色（用于兼容nodeColor，两者配置一处即可）|String	|否	|-	|
|stroke	|边框颜色（用于兼容nodeColor，两者配置一处即可）|String	|否	|-	|
|customClass|自定义class，多个逗号隔开，内置class可选（node-primary、node-success、node-warn、node-error、line-primary、line-success、line-warn、line-error、node-in-progress）|String	|否	|-		|
#### nodeInfoStyle 节点信息样式全局配置参数
|参数	|说明				|类型	|是否必须|默认值		|
|-	|-				|-	|-	|-		|
|top	|控制浮框位置，效果自己测试	|Number|否	|		|
|bottom	|控制浮框位置，效果自己测试	|Number|否	|-15		|
|left	|控制浮框位置，效果自己测试	|Number|否	|-5		|
|right	|控制浮框位置，效果自己测试	|Number|否	|		|
|class	|自定义class			|String	|否	|""		|
|style	|自定义样式			|String	|否	|""		|
|width	|浮动框宽度			|String	|否	|300px		|
|minWidth|浮动框最小宽度			|String	|否	|100px		|
|height	|浮动框高度			|String	|否	|200px		|
|minHeight|浮动框最小高度		|String	|否	|100px		|
|border	|浮动框边框			|String	|否	|2px solid #409eff	|
|padding|浮动框内边距			|String	|否	|5px		|

### Events 事件
|events			|说明			|参数				|
|-			|-			|-				|
|selection-changed	|元素选中事件		|{newSelection, oldSelection}	|
|element-hover		|元素hover事件		|event, element			|
|element-out		|元素out事件		|event, element			|


### Method 方法
|method			|说明		|参数		|返回值		|
|-			|-		|-		|-		|
|getBpmnViewer		|获取Viewer对象	|-		|Viewer		|
|getOverlays		|获取Overlays对象|-		|Overlays	|
|toggleSimulationMode	|开启或关闭模拟	|-		|-		|
|setAdaptiveScreen	|自适应屏幕	|-		|-		|
|setNodeColor		|设置节点颜色	|nodeColors	|-		|
|setNodeInfo		|设置节点信息	|nodeInfos	|-		|
|addOverlay		|添加节点浮框	|nodeInfo/nodeInfos|-		|
|addComment		|添加节点comment	|{id: 'xx', comments: ['xx','xx',...]}|-	|
|setSerialNumber	|添加节点序列号	|nodeInfos	|-		|

### Slot 插槽
|slot| 				|
|-		|-			|
|name		|说明			|
|toolbar-first	|按钮区的前面	|
|toolbar-last	|按钮区的后面	|
